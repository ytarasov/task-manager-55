package ru.t1.ytarasov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;
import ru.t1.ytarasov.tm.dto.model.UserDTO;

public interface IAuthService {

    String login(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull UserDTO check(@Nullable String login, @Nullable String password) throws Exception;

    void logout(@Nullable final SessionDTO session) throws Exception;

    @NotNull
    @SneakyThrows
    SessionDTO validateToken(@Nullable String token);

}
